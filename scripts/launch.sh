#!/bin/sh
DEFAULT_JAVA_OPTS="-server"
DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -Xmx1G -XX:+HeapDumpOnOutOfMemoryError"
DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -XX:+UseG1GC -XX:+ParallelRefProcEnabled"
DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -XX:MaxInlineLevel=20 -XX:MaxTrivialSize=12 -XX:-UseBiasedLocking"
COMPILER_OPTS="-Xss100M $DEFAULT_JAVA_OPTS $JAVA_OPTS"

#Assuming all Gatling installation in same path (with write permissions)
GATLING_RUNNER="java --add-opens java.base/jdk.internal.misc=ALL-UNNAMED -Dio.netty.tryReflectionSetAccessible=true $DEFAULT_JAVA_OPTS $JAVA_OPTS -jar loadtest-gatling-0.1.jar"

#No need to change this
GATLING_REPORT_DIR=/informes/results
GATHER_REPORTS_DIR=/informes/generados
 
#Change to your simulation class name
SIMULATION_NAME='com.jvt.testing.BasicSimulation'


if [ -d "$GATLING_REPORT_DIR" ]; then
echo "Directory already exists" ;
else
`mkdir -p $GATLING_REPORT_DIR`;
echo "$GATLING_REPORT_DIR directory is created"
fi

if [ -d "$GATHER_REPORTS_DIR" ]; then
echo "Directory already exists" ;
else
`mkdir -p $GATHER_REPORTS_DIR`;
echo "$GATHER_REPORTS_DIR directory is created"
fi


HOST=$(date +%N)
echo "Running simulation on host: $HOST"
$GATLING_RUNNER -nr -rf $GATLING_REPORT_DIR -s $SIMULATION_NAME 

echo "Searching files in $GATLING_REPORT_DIR"
find $GATLING_REPORT_DIR -type f  -exec \
   sh -c "ls -t $GATLING_REPORT_DIR  {} | head -n 1 | xargs -I {} cp {} $GATHER_REPORTS_DIR" \; 

echo "Rename simulation"
for i in `find $GATHER_REPORTS_DIR -name 'simulation.log'` ; do  mv $GATHER_REPORTS_DIR/simulation.log $GATHER_REPORTS_DIR/simulation-${HOST}.log   ; done

echo "Read  $GATHER_REPORTS_DIR"
ls  -la $GATHER_REPORTS_DIR

echo "Read $GATLING_REPORT_DIR"
ls -la $GATLING_REPORT_DIR
