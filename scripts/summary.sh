#!/bin/bash

DEFAULT_JAVA_OPTS="-server"
DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -Xmx1G -XX:+HeapDumpOnOutOfMemoryError"
DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -XX:+UseG1GC -XX:+ParallelRefProcEnabled"
DEFAULT_JAVA_OPTS="${DEFAULT_JAVA_OPTS} -XX:MaxInlineLevel=20 -XX:MaxTrivialSize=12 -XX:-UseBiasedLocking"
COMPILER_OPTS="-Xss100M $DEFAULT_JAVA_OPTS $JAVA_OPTS"

GATLING_RUNNER="java --add-opens java.base/jdk.internal.misc=ALL-UNNAMED -Dio.netty.tryReflectionSetAccessible=true $DEFAULT_JAVA_OPTS $JAVA_OPTS -jar loadtest-gatling-0.1.jar"
GATHER_REPORTS_DIR=/informes/generados



echo "Aggregating simulations"
$GATLING_RUNNER -Dgatling.core.directory.results=/gatling -ro $GATHER_REPORTS_DIR

echo "Copying files"
sshpass -p "X2406prk7j" rsync -qaPHv -e "ssh -o StrictHostKeyChecking=no" $GATHER_REPORTS_DIR/* ubuntu@192.168.60.2:/home/ubuntu/generados

echo "Delete"
rm -rf $GATHER_REPORTS_DIR/*
rm -rf /informes/results/*
