package com.jvt.testing

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BasicSimulation extends Simulation { // 3

  val httpProtocol = http // 4
    .baseUrl("http://10.152.183.33:8080") // 5
    .acceptHeader("application/json;q=0.9,*/*;q=0.8") // 6
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  val scn = scenario("BasicSimulation") // 7
    .exec(http("request_1") // 8
      .get("/")) // 9
    .pause(5) // 10

//  setUp( // 11
//    scn.inject(atOnceUsers(1)) // 12
//  ).protocols(httpProtocol) // 13

setUp(
  scn.inject(
     rampConcurrentUsers(0) to (2400) during (10 minutes),
     constantConcurrentUsers(2400) during (5 minutes)
  ).protocols(httpProtocol)
)

}
