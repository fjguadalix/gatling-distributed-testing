#!/bin/sh
ID_NAME="localhost:32000/kubectl:registry"
sudo docker pull bitnami/kubectl
ID_IMAGE=$(sudo docker images | grep bitnami/kubectl| awk {'print $3'})
echo $ID_IMAGE $ID_NAME
microk8s ctr images rm $ID_NAME
sudo docker tag $ID_IMAGE $ID_NAME
sudo docker push $ID_NAME
