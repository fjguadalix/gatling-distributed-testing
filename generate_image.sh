#!/bin/sh
ID_NAME="localhost:32000/loadtest-gatling:registry"
ID_IMAGE=$(sudo docker images | grep loadtest-gatling| awk {'print $3'})
if [ -z "$ID_IMAGE" ]
then
      echo "\$ID_IMAGE is empty"
else
      echo "\$ID_IMAGE is NOT empty"
      sudo docker rmi $ID_IMAGE
fi
sudo docker build . -t $ID_NAME
ID_IMAGE=$(sudo docker images | grep loadtest-gatling| awk {'print $3'})
echo $ID_IMAGE $ID_NAME
microk8s ctr images rm $ID_NAME
sudo docker tag $ID_IMAGE $ID_NAME
sudo docker push $ID_NAME
