# gatling-distributed-testing


[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Minimal [gatling-distributed-testing](https://gitlab.com/fjguadalix/gatling-distribute-testing/) sample project.

![plot](./images/Imagem-3.png)

## Requirements

For building and running the application you need:

- [Maven 3](https://maven.apache.org)
- [Docker](https://www.docker.com/)
- [Microk8s](https://microk8s.io/)

### Option 1: Scala scripts

Basic scala script is located under src/main/scala/com/jvt/testing

### Option 2: Building a non-native OCI Images

To create a non-native OCI docker image, simply run:

```sh
generate_image.sh
```

### Option 3: Before deploying

Exceute the following command to create a namespace named loadtest-gatling

```sh
 kubectl apply -f loadtest-gatling-namespace.yaml
```
Create a pvc associated to namespace loadtest-gatling

```sh
 kubectl apply -f loadtest-gatling-pvc.yaml
```
Take into account if you're not creating the pvc in microk8s, edit the file and delete the following line

```  
  storageClassName: microk8s-hostpath
```
If not the volume will not be created.

## Option 4: Gather reports directory

All the reports of the execution are stored in the pvc volume, after the report is generated the files are copied over sftp to another machine.

You'll need to edit the script located under scripts/summary.sh to change the user, password , host.

### Option 5: Deploy to kubernetes

Run the following command for deployment

```sh
 deploy.sh
```

## Option 6: Viewing report

Once all pods are finished the report will be stored on the location specified previously.


## Copyright

Copyright 2021 Francisco Javier Guadalix Frax

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
