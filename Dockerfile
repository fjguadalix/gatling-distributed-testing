## gatling project build as uberjar
FROM maven:3.6-jdk-11-slim AS java-build

WORKDIR .
ADD src src
ADD pom.xml pom.xml

RUN mvn clean package

## java (run)
FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.3_7

RUN apk add --no-cache --update bash util-linux coreutils curl tar openssh rsync sshpass && rm -rf /var/cache/apk/*


WORKDIR .
COPY --from=java-build ./target/loadtest-gatling-0.1.jar loadtest-gatling-0.1.jar

ADD scripts/launch.sh launch.sh
RUN chmod +x launch.sh
ADD scripts/summary.sh summary.sh
RUN chmod +x summary.sh

#CMD ["/launch.sh"]
